/**
 * View Models used by Spring MVC REST controllers.
 */
package prepper.web.rest.vm;
