package prepper.web.rest.supply;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import prepper.security.AuthoritiesConstants;
import prepper.service.SupplyService;
import prepper.service.dto.supply.SupplyDto;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/supply")
public class SupplyResource {
    private final Logger log = LoggerFactory.getLogger(SupplyResource.class);

    @Autowired
    private SupplyService supplyService;

    @PostMapping("/save")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.USER + "\")")
    public SupplyDto createOrUpdateSupply(@Valid @RequestBody SupplyDto supplyDto) throws URISyntaxException {
        log.debug("REST request to save Supply : {}", supplyDto);
        SupplyDto savedSupplyDto = supplyService.saveSupply(supplyDto);
        return savedSupplyDto;
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.USER + "\")")
    public void deleteSupply(@Valid @PathVariable("id") Long supplyId) throws URISyntaxException {
        log.debug("REST request to delete Supply with ID : {}", supplyId);
        supplyService.deleteSupply(supplyId);
    }

    @GetMapping("/list/{userId}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<SupplyDto>> getSuppliesForUser(@Valid @PathVariable("userId") Long userId,
                                                              Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get supplies by user ID : {}", userId);
        Page<SupplyDto> page = supplyService.findByUserId(pageable, userId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
