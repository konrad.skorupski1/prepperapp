package prepper.repository.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import prepper.domain.supplies.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
