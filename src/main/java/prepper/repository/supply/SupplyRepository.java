package prepper.repository.supply;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import prepper.domain.supplies.Supply;

@Repository
public interface SupplyRepository extends JpaRepository<Supply, Long> {
    Page<Supply> findByUserId(Pageable pageable, Long userId);
}
