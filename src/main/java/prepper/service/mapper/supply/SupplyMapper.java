package prepper.service.mapper.supply;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import prepper.domain.supplies.Product;
import prepper.domain.supplies.Supply;
import prepper.repository.UserRepository;
import prepper.repository.product.ProductRepository;
import prepper.repository.supply.SupplyRepository;
import prepper.service.dto.product.ProductDto;
import prepper.service.dto.supply.SupplyDto;
import prepper.service.mapper.product.ProductMapper;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SupplyMapper {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SupplyRepository supplyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductMapper productMapper;

    public Supply toEntity(SupplyDto dto) {
        Supply supply;
        if(dto.getId() != null) {
            supply = supplyRepository.getOne(dto.getId());
        } else {
            supply = new Supply();
        }
        supply.setUser(userRepository.getOne(dto.getUserId()));
        ProductDto productDto = dto.getProductDto();
        if(productDto != null) {
            if(productDto.getId() != null) {
                supply.setProduct(productRepository.getOne(productDto.getId()));
            } else {
                supply.setProduct(productMapper.toEntity(productDto));
            }
        }
        supply.setAmount(dto.getAmount());
        supply.setExpirationDate(dto.getExpirationDate());
        return supply;
    }

    public SupplyDto toDto(Supply supply) {
        SupplyDto supplyDto = new SupplyDto();
        supplyDto.setId(supply.getId());
        supplyDto.setProductDto(productMapper.toDto(supply.getProduct()));
        supplyDto.setAmount(supply.getAmount());
        supplyDto.setExpirationDate(supply.getExpirationDate());
        supplyDto.setUserId(supply.getUser().getId());
        return supplyDto;
    }

    public List<Supply> toEntities(List<SupplyDto> dto) {
        return dto.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<SupplyDto> toDtos(List<Supply> dto) {
        return dto.stream().map(this::toDto).collect(Collectors.toList());
    }
}
