package prepper.service.mapper.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import prepper.domain.supplies.Product;
import prepper.repository.product.ProductRepository;
import prepper.service.dto.product.ProductDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    @Autowired
    private ProductRepository productRepository;

    public Product toEntity(ProductDto dto) {
        Product product;
        if(dto.getId() != null) {
            product = productRepository.getOne(dto.getId());
        } else {
            product = new Product();
        }
        product.setId(dto.getId());
        product.setName(dto.getName());
        product.setBrand(dto.getBrand());
        product.setGrossWeight(dto.getGrossWeight());
        product.setNetWeight(dto.getNetWeight());
        product.setProtein(dto.getProtein());
        product.setCarbohydrates(dto.getCarbohydrates());
        product.setFat(dto.getFat());
        return product;
    }

    public ProductDto toDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setBrand(product.getBrand());
        productDto.setGrossWeight(product.getGrossWeight());
        productDto.setNetWeight(product.getNetWeight());
        productDto.setProtein(product.getProtein());
        productDto.setCarbohydrates(product.getCarbohydrates());
        productDto.setFat(product.getFat());
        return productDto;
    }

    public List<Product> toEntities(List<ProductDto> dto) {
        return dto.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<ProductDto> toDtos(List<Product> dto) {
        return dto.stream().map(this::toDto).collect(Collectors.toList());
    }
}
