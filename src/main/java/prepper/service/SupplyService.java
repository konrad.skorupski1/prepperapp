package prepper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import prepper.domain.supplies.Supply;
import prepper.repository.supply.SupplyRepository;
import prepper.service.dto.supply.SupplyDto;
import prepper.service.mapper.supply.SupplyMapper;

@Service
public class SupplyService {

    @Autowired
    private SupplyRepository supplyRepository;

    @Autowired
    private SupplyMapper supplyMapper;

    public SupplyDto saveSupply(SupplyDto supplyDto) {
        Supply supply = supplyMapper.toEntity(supplyDto);
        supply = supplyRepository.save(supply);
        return supplyMapper.toDto(supply);
    }

    public void deleteSupply(Long id) {
        supplyRepository.deleteById(id);
    }

    public Page<SupplyDto> findByUserId(Pageable pageable, Long userId) {
        return supplyRepository.findByUserId(pageable, userId).map(supplyMapper::toDto);
    }
}
