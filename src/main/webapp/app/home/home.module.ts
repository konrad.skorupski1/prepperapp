import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PrepperappSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [PrepperappSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class PrepperappHomeModule {}
