import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { PrepperappSharedModule } from 'app/shared/shared.module';
import { PrepperappCoreModule } from 'app/core/core.module';
import { PrepperappAppRoutingModule } from './app-routing.module';
import { PrepperappHomeModule } from './home/home.module';
import { PrepperappEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { SupplyModule } from 'app/supply/supply.module';

@NgModule({
  imports: [
    BrowserModule,
    PrepperappSharedModule,
    PrepperappCoreModule,
    PrepperappHomeModule,
    SupplyModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    PrepperappEntityModule,
    PrepperappAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class PrepperappAppModule {}
