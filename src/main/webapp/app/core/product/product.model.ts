export class Product {
  constructor(
    public id: number,
    public name: string,
    public brand: string,
    public grossWeight: number,
    public netWeight: number,
    public protein: number,
    public carbohydrates: number,
    public fat: number
  ) {}
}
