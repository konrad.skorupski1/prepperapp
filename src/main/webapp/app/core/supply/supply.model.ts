import { Product } from 'app/core/product/product.model';

export class Supply {
  constructor(public id: number, public productDto: Product, public userId: number, public amount: number, public expirationDate: Date) {}
}
