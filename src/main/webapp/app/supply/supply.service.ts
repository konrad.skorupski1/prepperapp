import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { Supply } from 'app/core/supply/supply.model';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';

@Injectable({ providedIn: 'root' })
export class SupplyService {
  public resourceUrl = SERVER_API_URL + 'api/supply';

  constructor(private http: HttpClient) {}

  save(supply: Supply): Observable<{}> {
    return this.http.post(SERVER_API_URL + 'api/supply/save', supply);
  }

  query(userId?: number, req?: Pagination): Observable<HttpResponse<Supply[]>> {
    const options = createRequestOption(req);
    return this.http.get<Supply[]>(this.resourceUrl + '/list/' + userId, { params: options, observe: 'response' });
  }
}
