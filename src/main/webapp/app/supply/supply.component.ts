import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SupplyService } from './supply.service';
import { Supply } from 'app/core/supply/supply.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { AccountService } from 'app/core/auth/account.service';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'jhi-supplies',
  templateUrl: './supply.component.html'
})
export class SupplyComponent implements OnInit {
  supplies: Supply[] | null = null;

  //table paging
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  previousPage!: number;
  ascending!: boolean;

  supplyForm = this.fb.group({
    id: [''],
    productId: ['', [Validators.required]],
    amount: ['', [Validators.required]],
    expirationDate: ['']
  });

  constructor(
    private accountService: AccountService,
    private supplyService: SupplyService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data
      .pipe(
        flatMap(
          () => this.accountService.identity(),
          data => {
            this.page = 1;
            this.previousPage = 0;
            this.ascending = true;
            this.predicate = 'id';
            this.loadSupplies();
          }
        )
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.loadSupplies();
  }

  private loadSupplies(): void {
    this.supplyService
      .query(this.accountService.getUserId(), {
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<Supply[]>) => this.onSuccess(res.body, res.headers));
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private onSuccess(supplies: Supply[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.supplies = supplies;
  }

  trackIdentity(index: number, item: Supply): any {
    return item.id;
  }

  loadPage(page: number): void {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }
}
