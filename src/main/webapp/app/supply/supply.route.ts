import { Route } from '@angular/router';

import { SupplyComponent } from './supply.component';

export const supplyRoute: Route = {
  path: 'supplies',
  component: SupplyComponent,
  data: {
    authorities: [],
    pageTitle: 'supplies.title'
  }
};
