import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PrepperappSharedModule } from 'app/shared/shared.module';
import { supplyRoute } from 'app/supply/supply.route';
import { SupplyComponent } from 'app/supply/supply.component';

@NgModule({
  imports: [PrepperappSharedModule, RouterModule.forChild([supplyRoute])],
  declarations: [SupplyComponent]
})
export class SupplyModule {}
